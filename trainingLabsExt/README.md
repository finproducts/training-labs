
# Pod Template

Use this example to easily create pods for [FIN Stack](http://j2inn.com) and
other haystack systems.

This template consists of a [Grunt](http://gruntjs.com/) configuration that
includes tasks for creating and packaging a pod file, and some examples of
common pod resources like .trio resource files and fantom extensions.

### Setup

To get started, first make sure you have [node](http://nodejs.org/) installed.
Then open a terminal window and type `npm i -g grunt-cli` to install the Grunt
command line runner (you may have to prefix the command with `sudo` if your
node installation complains about permission issues). Then, `cd` to the project
directory and run `npm install` to download the needed dependancies.


### Configuration

Most of the configuration happens in `config.js` in the root of the repository.
Edit this file to change the name of the pod, the version and various other
properties.


### Buildling

Once configured, run `grunt pod` to build a pod from the files in the `src/`
directory.  The resultant pod will be in `dist/<pod name>.pod`.

### Publishing

Now that you have your pod, run `grunt publish` to upload the pod to
[Stackhub](http://stackhub.org/).  By default, `grunt publish` pushes the pod
to Stackhub's [Sandbox](http://sandbox.stackhub.org/) environment.  Once you've
ensured the pod is working correctly use `grunt publish:production` to push the
pod to Sandbox's production environment.



