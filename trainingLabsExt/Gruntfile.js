/*global module:false*/
module.exports = function(grunt) {

    //handles plugins
    require('jit-grunt')(grunt);

    var config = require('./config.js');

    // Project configuration.
    grunt.initConfig({

        clean: {
            dist: ['dist/'],
            temp: ['temp/'],
        },

        compress: {
            pod: {
                options: {
                    archive: 'dist/' + config.name + '.pod',
                    mode: 'zip',
                },
                files: [
                    {expand: true, cwd: 'src/', src: '**/*'},
                    {expand: true, cwd: 'temp/', src: '**/*'}
                ]

            }
        },

        'stackhub-publish': {
            options: {
                shuser: config.stackhub.username,
                shpass: config.stackhub.password,
                releaseStatus: config.stackhub.releaseStatus,
                filepath: 'dist/'+config.name+'.pod'
            },
            sandbox: {
                options: {
                    host: 'http://sandbox.stackhub.org'
                }
            },
            production: {
                options: {
                    host: 'http://stackhub.org'
                }
            },
        }

    });

    var r = grunt.registerTask;

    r('default', [/* put your normal build tasks here*/]);

    r('publish', ['stackhub-publish:sandbox']);
    r('publish:production', ['stackhub-publish:production']);

    r('pod', ['generateProps', 'compress:pod', 'clean:temp']);

    r('generateProps', function() {

        var merge = require('merge').recursive;

        var defaults = {

            meta: {
                pod: {
                    name: config.name,
                    version: config.version,
                    isScript: false,
                    depends: config.depends || "sys 1.0",
                    summary: config.description || config.summary,
                    build: {
                        ts: new Date().toISOString(),
                    },
                    org: {
                        name: config.organization.name,
                        uri: config.organization.url,
                    }
                },
                license: {
                    name: 'Commercial',
                },
                ext: {
                    name: config.name.replace(/Ext$/, ''),
                    icon24: 'fan://'+config.name+'/res/img/icon24.png',
                    icon72: 'fan://'+config.name+'/res/img/icon72.png',
                },
            },

            index: {
                proj: {
                    ext: config.name
                }
            },

        };

        function gen(obj, prefix) {
            var ret = '';
            for(var key in obj) {
                if(obj.hasOwnProperty(key)) {
                    var value = obj[key];
                    if(typeof value === 'object')
                        ret += gen(value, key + '.');
                    else if(value !== null) {
                            ret += prefix + key + '=' + value + '\n';
                    }
                }
            }
            return ret;
        }

        grunt.file.write('temp/meta.props', gen(merge(defaults.meta, config.meta)));
        grunt.file.write('temp/index.props', gen(merge(defaults.index, config.index)));

    });

};

