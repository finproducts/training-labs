
module.exports = {

    name: 'finTrainingLabsExt', // pod name has to end in "Ext"
    version: '0.1.0',
    description: 'Labs used for learning how to use FINStack',
    depends: '',
    repository: 'http://github.com/<your repo here>',

    organization: {
        name: 'J2 Innovations',
        url: 'http://j2inn.com'
    },

    stackhub: {
        username: '',
        password: '',
        releaseStatus: 'beta',
    },

    // uncomment these objects to override properties on props files explicity
    // (not needed for most configurations)
    /*
    meta: {

        proj: {
           name: 'FooProduct/Company',
        },

        ext: {
            depends: "point,his"
        }

    },
   */

    /*
    // properties for `index.props` file goes here
    index: {

        sys: {
            envProps: 'myPod'
        }

    }
    */

}

